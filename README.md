# 云喵圈子-uniapp

#### 介绍

社区圈子程序，前端使用uniapp开发，可同时运行与H5,小程序,APP


#### 软件架构

前端：uniapp

后端：ThinkPHP6


#### 目录结构

1.  service_php 为后台目录

2.  uniapp 为前端目录

#### 使用说明

1.  后端：根据ThinkPHP运行环境搭配即可，第一次运行后进行安装

2.  前端：在前端目录中：common\js\config.js中修改相应配置即可

更多详细说明：https://www.tn721.cn/p/66.html

#### 码云Gitee

码云地址：[点击访问](https://gitee.com/ym721/ym_uni)

#### 其他

特别鸣谢 uView UI框架：https://ext.dcloud.net.cn/plugin?id=1593

本软件持续更新用，如果觉得不错，到gitee点个 star吧

欢迎对此程序提出宝贵的意见。

#### 作者联系方式

微信：lurenbf